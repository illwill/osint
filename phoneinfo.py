# lookup phone info from the phonenumbers lib and the freecnam.org site for callerid info
# can either type the number inline or interactive if no number provided
# i.e. python3 phoneinfo.py or python3 phoneinfo.py 213555555

# lookup phone info from the phonenumbers lib and the freecnam.org site for callerid info
# can either type the number inline or interactive if no number provided
# i.e. python3 phoneinfo.py or python3 phoneinfo.py 213555555

import phonenumbers
from phonenumbers import geocoder, carrier, timezone
from phonenumbers.phonenumberutil import region_code_for_number
import requests
import sys
import re
from bs4 import BeautifulSoup

red = '\033[31m'
blue = '\033[34m'
reset = '\033[0m'

def print_blue_numbers(text):
    text = re.sub(r'(\d)', lambda match: f"{blue}{match.group(0)}{reset}", text)
    print(text)

ascii_art = r"""
               _.===========================._
            .'`  .-  - __- - - -- --__--- -.  `'.
        __ / ,'`     _|--|_________|--|_     `'. \
      /'--| ;    _.'\ |  '         '  | /'._    ; |
     //   | |_.-' .-'.'    -  -- -    '.'-. '-._| |
    (\)   \"` _.-` /                     \ `-._ `"/
    (\)    `-`    /      .---------.      \    `-`
    (\)           |      ||1||2||3||      |
   (\)            |      ||4||5||6||      |
  (\)             |      ||7||8||9||      |
 (\)           ___|      ||*||0||#||      |
 (\)          /.--|      '---------'      |
  (\\)        (\) |\_  _  __   _   __  __/|
 (\)        (\)   |                       |
(\)_._._.__(\)    |                       |
 (\\\\ill\\\)      '.___________________.'
  '-'-'-'--'
"""
me = f"""                       -=[{blue}PHONEINFO{reset}]=-
                         by: illwill
"""
print_blue_numbers(ascii_art)
print(me)

def get_caller_id(phone_number):
    url = f"https://freecnam.org/dip?q={phone_number}"
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.text.strip()
    except requests.RequestException as e:
        return None

def get_html_phone_info(phone_number):
    url = f"http://whocalld.com/+1{phone_number}"
    response = requests.get(url)
    
    if response.status_code != 200:
        raise Exception(f"Failed to fetch data: {response.status_code}")
    
    soup = BeautifulSoup(response.text, 'html.parser')
    
    location = soup.find('span', class_='location').text.strip()
    page_content = soup.find('div', class_='page').text.strip()
    
    if "mobile phone" in page_content:
        phone_type = "mobile phone"
    elif "virtual phone" in page_content:
        phone_type = "virtual phone"
    else:
        phone_type = "unknown"
    
    carrier_info = page_content.split("The carrier for this number is")[1].split('in')[0].strip()
    
    return {
        'type': phone_type,
        'carrier': carrier_info,
        'location': location
    }

def get_phone_number_details(phone_number):
    try:
        parsed_number = phonenumbers.parse(phone_number, None)
    except phonenumbers.phonenumberutil.NumberParseException:
        try:
            parsed_number = phonenumbers.parse(phone_number, 'US')
        except phonenumbers.phonenumberutil.NumberParseException as e:
            print(f"Failed to parse number: {str(e)}")
            return
    
    if not phonenumbers.is_valid_number(parsed_number):
        print("The provided number is not valid.")
        return
    
    country_code = region_code_for_number(parsed_number)
    if country_code != "US":
        caller_id = "Non-US number"
    else:
        caller_id = get_caller_id(phone_number.strip('+')) or None
    
    country = phonenumbers.geocoder.country_name_for_number(parsed_number, "en") or None
    country_display = f"{country} ({country_code})" if country else None
    
    location = geocoder.description_for_number(parsed_number, "en") or None
    phone_carrier = carrier.name_for_number(parsed_number, "en") or None
    phone_timezones = timezone.time_zones_for_number(parsed_number)
    phone_timezone = phone_timezones[0] if phone_timezones and phone_timezones[0] != "Etc/Unknown" else None
    
    try:
        html_info = get_html_phone_info(phone_number.strip('+'))
    except Exception as e:
        html_info = {'type': 'unknown', 'carrier': None, 'location': None}
    
    print(f"{blue}Number:{reset}   {phone_number}")
    print(f"{blue}Type:{reset}     {html_info['type']}")
    
    if caller_id:
        print(f"{blue}CallerID:{reset} {caller_id}")
    
    if phone_carrier or html_info['carrier']:
        carriers = " / ".join(filter(None, [phone_carrier, html_info['carrier']]))
        print(f"{blue}Carrier:{reset}  {carriers}")
    else:
        print(f"{blue}Carrier:{reset}  {red}Not found{reset}")
    
    if location or html_info['location']:
        locations = " / ".join(filter(None, [location, html_info['location']]))
        print(f"{blue}Location:{reset} {locations}")
    else:
        print(f"{blue}Location:{reset} {red}Not found{reset}")
    
    if country_display:
        print(f"{blue}Country:{reset}  {country_display}")
    
    if phone_timezone:
        print(f"{blue}Timezone:{reset} {phone_timezone}")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        input_phone_number = " ".join(sys.argv[1:])
    else:
        input_phone_number = input("Enter a phone number (include country code): ")
    normalized_phone_number = re.sub(r'[\-\(\)\s]', '', input_phone_number)
    get_phone_number_details(normalized_phone_number)