# OSINT



## Tools

Some scripts/programs etc I made to get some info on a targets
[[_TOC_]]
## phoneinfo
- [ ][phoneinfo.py](phoneinfo.py) - Python script to automate getting information on a phone number. Will attempt to return CallerID on US Numbers from freecnam.org.  
![](images/phoneinfo.png)

## SSID2Wigle
- [ ][wigle.py](wigle.py) - Python script to automate getting information on a SSID. Will attempt to return encryption,location, google maps link from any matching SSIDs found wigle.net.
![](images/wigle.png)

