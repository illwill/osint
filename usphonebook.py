import cloudscraper
from bs4 import BeautifulSoup
import argparse
import json
import random

def print_banner():
    banner = """
\033[94m
  _    _  _____ _____  _                      _                 _    
 | |  | |/ ____|  __ \| |                    | |               | |   
 | |  | | (___ | |__) | |__   ___  _ __   ___| |__   ___   ___ | | __
 | |  | |\___ \|  ___/| '_ \ / _ \| '_ \ / _ \ '_ \ / _ \ / _ \| |/ /
 | |__| |____) | |    | | | | (_) | | | |  __/ |_) | (_) | (_) |   < 
  \____/|_____/|_| ___|_| |_|\___/|_| |_|\___|_.__/ \___/ \___/|_|\_\\
                  / ____|                                            
                 | (___   ___ _ __ __ _ _ __   ___ _ __              
                  \___ \ / __| '__/ _` | '_ \ / _ \ '__|             
                  ____) | (__| | | (_| | |_) |  __/ |                
                 |_____/ \___|_|  \__,_| .__/ \___|_|                
                                       | |                           
                                       |_| 
                    ___________
                   /.---------.\`-._
                  //          ||    `-._
                  || `-._     ||        `-._
                  ||     `-._ ||            `-._
                  ||    _____ ||`-._            \\
            _..._ ||   | __ ! ||    `-._        |
          _/     \||   .'  |~~||        `-._    |
      .-``     _.`||  /   _|~~||    .----.  `-._|
     |      _.`  _||  |  |23| ||   / :::: \    \\
     \ _.--`  _.` ||  |  |56| ||  / ::::: |    |
      |   _.-`  _.||  |  |79| ||  |   _..-'   /
      _\-`   _.`O ||  |  |_   ||  |::|        |
    .`    _.`O `._||  \    |  ||  |::|        |
 .-`   _.` `._.'  ||   '.__|--||  |::|        \\
`-._.-` \`-._     ||   | ":  !||  |  '-.._    |
         \   `--._||   |_:"___||  | ::::: |   |
          \  /\   ||     ":":"||   \ :::: |   |
           \(  `-.||       .- ||    `.___/    /
           |    | ||   _.-    ||              |
           |    / \\.-________\\____.....-----'
           \    -.      \ |         |
            \     `.     \ \        | 
 __________  `.    .'\    \|        |\  _________
               `..'   \    |        | \          
                \\   .'    |       /  .`.
                | \.'      |       |.'   `-._
                 \     _ . /       \_\-._____)
                  \_.-`  .`'._____.'`.
                    \_\-|             |
                         `._________.'
\033[0m
\033[93m                 by -=[illwill]=-\033[0m
"""
    print(banner)



def create_headers():
    user_agents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Safari/605.1.15'
    ]
    headers = {
        'User-Agent': random.choice(user_agents),
        'Accept-Language': 'en-US,en;q=0.9',
        'Accept-Encoding': 'gzip, deflate',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Referer': 'https://www.google.com/',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1'
    }
    return headers

def usphonebook(phone=None, fullname=None, state=None, city=None):
    def encode_args(args):
        parts = args.split(' ')
        return '-'.join(parts)

    def phonebook_search(usphonebook_link):
        target_links = set()
        headers = create_headers()

        scraper = cloudscraper.create_scraper(allow_brotli=False)
        response = scraper.get(usphonebook_link, headers=headers)

        if response.status_code != 200:
            return list(target_links)

        soup = BeautifulSoup(response.text, 'html.parser')

        for link in soup.find_all('input', attrs={'name': 'link'}):
            value = link.get('value')
            if value.startswith('/'):
                value = f"https://usphonebook.com{value}"
            target_links.add(value)

        for a_tag in soup.find_all('a', class_='ls_contacts-btn', href=True):
            href = a_tag['href']
            if href.startswith('/'):
                href = f"https://usphonebook.com{href}"
            target_links.add(href)

        return list(target_links)

    def phonebook_target(target_links):
        persons = []
        headers = create_headers()

        scraper = cloudscraper.create_scraper(allow_brotli=False)
        processed_urls = set()
        for url in target_links:
            if url in processed_urls:
                continue
            processed_urls.add(url)

            response = scraper.get(url, headers=headers)

            if response.status_code != 200:
                continue

            soup = BeautifulSoup(response.text, 'html.parser')
            for script in soup.find_all('script', type='application/ld+json'):
                script_text = script.string
                if '@type' in script_text:
                    try:
                        data = json.loads(script_text)
                        if 'givenName' in data:
                            persons.append(data)
                    except json.JSONDecodeError as e:
                        continue

        return persons

    usphonebook_link = ''
    target_links = []
    persons = []

    if fullname:
        encoded_fullname = encode_args(fullname)
        if not state:
            return []
        if city:
            encoded_city = encode_args(city)
            usphonebook_link = f"https://usphonebook.com/{encoded_fullname}/{state}/{encoded_city}"
        else:
            usphonebook_link = f"https://usphonebook.com/{encoded_fullname}/{state}"

    if phone:
        phone = f"{phone[:3]}-{phone[3:6]}-{phone[6:]}" if phone and '-' not in phone else phone
        usphonebook_link = f"https://usphonebook.com/{phone}"

    target_links = phonebook_search(usphonebook_link)
    if not target_links:
        target_links.append(usphonebook_link)

    persons = phonebook_target(target_links)

    return persons

def main():
    parser = argparse.ArgumentParser(description='USPhonebook Scraper')
    parser.add_argument('--phone', help='Specify a phone number [123-456-7890]')
    parser.add_argument('--fullname', help='Specify the full name of the target [John Smith]')
    parser.add_argument('--state', help='Specify the state the target resides [New York]')
    parser.add_argument('--city', help='Specify the city the target resides [Brooklyn]')
    parser.add_argument('--usage', action='store_true', help='Show usage')

    args = parser.parse_args()

    if args.usage or (not args.phone and not args.fullname):
        parser.print_usage()
        return

    results = usphonebook(phone=args.phone, fullname=args.fullname, state=args.state, city=args.city)

    for person in results:
        print(f"\033[91m\033[1mLink:\033[0m https://{person.get('homeLocation', {}).get('url', '')}")
        print(f"\033[91m\033[1mName:\033[0m {person.get('name', '')}")
        home_address = person.get('homeLocation', {}).get('address', {})
        formatted_home_address = f"{home_address.get('streetAddress', '')} {home_address.get('addressRegion', '')} {home_address.get('addressLocality', '')} {home_address.get('postalCode', '')}"
        print(f"\033[91m\033[1mCurrent Address:\033[0m {formatted_home_address}")

        addresses = []
        for addr in person.get('address', []):
            formatted_address = f"{addr.get('streetAddress', '')} {addr.get('addressRegion', '')} {addr.get('addressLocality', '')} {addr.get('postalCode', '')}"
            addresses.append(formatted_address)
        print(f"\033[91m\033[1mPrevious Addresses:\033[0m {', '.join(addresses)}")

        related_persons = [related.get('name', '') for related in person.get('relatedTo', [])]
        print(f"\033[91m\033[1mRelated Persons:\033[0m {', '.join(related_persons)}")
        print(f"\033[91m\033[1mEmails:\033[0m {', '.join(person.get('email', []))}")
        print(f"\033[91m\033[1mTelephones:\033[0m {', '.join(person.get('telephone', []))}")
        print("\n")

if __name__ == "__main__":
    print_banner()
    main()