import requests
import argparse
import base64

# Add Hardcoded API Name and API Token from your Wigle Account
API_NAME = "AIDxxxxxxxxxxxxxxxxxx"
API_TOKEN = "zzzzzzzzzzzzzzz"


auth_string = f"{API_NAME}:{API_TOKEN}"
auth_bytes = auth_string.encode('ascii')
auth_base64 = base64.b64encode(auth_bytes).decode('ascii')
headers = {
    'Authorization': f'Basic {auth_base64}',
    'Accept': 'application/json'
}

def wigle_logo():
    yellow = "\033[93m"
    reset = "\033[0m"

    logo = """
                                    ./%@@&#/.                                   
                          &@@@@@@@@@@@@@@@@@@@@@@@@@@@#                         
                    .@@@@@@/***,,***,,,,,,,,,********@@@@@@@                    
                .@@@@&,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,%@@@@@                
             ,@@@@,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,@@@@@             
           @@@@,,,,,,,,,,,,,,,,,,,(&@@@@@@@@&(,,,,,,,,,,,,,,,,,,,&@@@@          
         @@@,,,,,,,,,,,,,,,%@@@@@@@@@@@@@@@@@@@@@@@@@,,,,,,,,,,,,,,,@@@@        
       @@@,,,,,,,,,,,,,&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@,,,,,,,,,,,,,@@@@      
     &@@*,,,,,,,,,,,@@@@@@@@@@@@@#,,,,,,,,,,,,/@@@@@@@@@@@@@,,,,,,,,,,,.@@@     
    @@@,,,,,,.,.,,@@@@@@@@@@*,,,,,,,,,,,,,,,,,,,,,.,@@@@@@@@@@,.,,,,,,,.,@@@@   
   @@@..........@@@@@@@@@.,..........................,,@@@@@@@@@..........%@@@  
  @@@..........@@@@@@@@..........(@@@@@@@@@@@@%..........@@@@@@@@*.........@@@@ 
 @@@@@&.....,@@@@@@@@@@.......@@@@@@@@@@@@@@@@@@@@.......@@@@@@@@@@.......@@@@@.
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@#.........%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@..........@@@@@@@@@@@@@
@@@@@@@@@@@@(............,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@.............@@@@@@@@@@@@
@@@@@@@@@%@@@@@.............@@@@@@@/.....@@@@@@@@@@@@.............@@@@@@/@@@@@@@
@@@@@@@@&.@@@@@@@.............@@@@#........&@@@@@@&............,@@@@@@@@@@@@.@@@
@@@@.@@@@@@@@@@@@@@.............%@@@@,,.......@@(............&@@@@@@@@@@@@@@*@@@
 @@@&@@@@@@@@@@@@@@@@/............,@@@@%...................@@@@@@@@@@@,@@@@.@@@@
 @@@**@@.@@@@@@@@@@@@@@@.............@@@@@...............@@@@@@@@@@@@..@@@@@@@@ 
  @@@@@@@&@@@@@@@@@@@@@@@@.........*@@@@@@@@@........./@@@@@@@@@@@@@@@@@@,@@@@@ 
   @@@@.@@.@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ,@@@&,@@@@  
    @@@,%@@@%.,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@,@@@&   
     #@@@&.@@.@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/@@@@/@@@@@     
       @@@@@@@@,@*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&@@@@@@,@@@@@      
         @@@@ &@@@ ,@&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@.@@%@@@@@@        
           @@@@@@@@@(&@@*%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@.@@@@@@@@@@@@%          
              @@@@@@(@@@#@@@@@@@@@@@@@@@@@@@@@@@@@@@,@@@@@@(@@&@@@@             
                 @@@@&@.#&@@@@@.*.@.@@@%,.,@@/@@@@@@@@@@@@@@@@@@                
                     @@@@@@@@@/*@@@@@@@@@@@@@@@@.@@.#@@@@@@@                    
                          #@@@@@@@@@@@@@@@@@@@@@@@@@@@*     
"""

    print(f"{yellow}{logo}{reset}")


def get_wigle_info(ssid):
    url = f"https://api.wigle.net/api/v2/network/search?ssid={ssid}"
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error: {response.status_code} - {response.text}")
        return None

def parse_results(results):
    for result in results:
        trilat = result.get('trilat')
        trilong = result.get('trilong')
        ssid = result.get('ssid')
        encryption = result.get('encryption')
        country = result.get('country')
        region = result.get('region')
        road = result.get('road')
        city = result.get('city')
        housenumber = result.get('housenumber')
        postalcode = result.get('postalcode')
        netid = result.get('netid')
        channel = result.get('channel')
        first_seen = result.get('firsttime')
        last_seen = result.get('lasttime')
        
        print(f"SSID: {ssid}")
        print(f"Encryption: {encryption}")
        print(f"MAC Address (NetID): {netid}")
        print(f"Channel: {channel}")
        print(f"First Seen: {first_seen}")
        print(f"Last Seen: {last_seen}")
        print(f"Location: {road}, {city}, {region}, {country}, {housenumber}, {postalcode}")
        print(f"GPS: {trilat}, {trilong}")
        print(f"Google Maps: https://www.google.com/maps?q={trilat},{trilong}")
        print("="*50)

if __name__ == "__main__":
    wigle_logo()
    print(f"                                   SSID2Wigle")
    print(f"                               -=[ by illwill ]=-")
    parser = argparse.ArgumentParser(description="Get information from Wigle.net by SSID.")
    parser.add_argument('ssid', type=str, help='The SSID to search for.')

    args = parser.parse_args()


    ssid_info = get_wigle_info(args.ssid)
    
    if ssid_info and ssid_info.get('results'):
        parse_results(ssid_info['results'])
    else:
        print("No results found or failed to process the image URL.")
